#ifndef ROOT__RANDOM_HXX
#define ROOT__RANDOM_HXX

#include <array>
#include <cstdio>
#include <fstream>
#include <limits>
#include <type_traits>

#include <Rtypes.h>
#include <TFile.h>
#include <TKey.h>
#include <TRandom.h>

#include <nxx/algorithm.hxx>
#include <nxx/random.hxx>

#include "root/types.hxx"

namespace root::random {

	using engine_seed_t = uint_t;

	namespace traits {

		template <typename Engine>
		using is_engine = std::is_base_of<TRandom, Engine>;

		template <typename S, typename Engine, typename = void>
		struct is_engine_seed_sequence : std::false_type {};

		template <typename S, typename Engine>
		struct is_engine_seed_sequence<
		    S, Engine,
		    typename std::enable_if<
		        nxx::random::traits::is_random_number_engine_seed_sequence<S, Engine>::value
		        && !std::is_same<typename std::remove_cv<S>::type,
		                         typename Engine::engine_type>::value>::type> : std::true_type {};

	}  // namespace traits

	template <typename Engine, typename = typename std::enable_if<traits::is_engine<Engine>::value>::type>
	class engine {
	public:
		using engine_type = Engine;
		using result_type = engine_seed_t;

		// FIXME: TRandom1 changes each default call
		engine() = default;

		explicit engine(engine_type const& engine)
		    : m_engine{engine} {}
		explicit engine(result_type seed)
		    : m_engine{seed} {}

		template <typename SeedSeq,
		          typename
		          = typename std::enable_if<traits::is_engine_seed_sequence<SeedSeq, engine>::value>::type>
		explicit engine(SeedSeq& seeds) {
			seed(seeds);
		}

		auto constexpr min() -> result_type const { return 1; }
		auto constexpr max() -> result_type const { return std::numeric_limits<result_type>::max(); }

		// TODO: TRandom1 has SetSeed2 as well
		void seed() { m_engine.SetSeed(); }
		void seed(result_type seed) { m_engine.SetSeed(seed); }

		// TODO: TRandom1 has SetSeeds
		template <typename SeedSeq,
		          typename
		          = typename std::enable_if<traits::is_engine_seed_sequence<SeedSeq, engine>::value>::type>
		void seed(SeedSeq& seeds) {
			auto ss = std::array<result_type, 1>{};
			seeds.generate(std::begin(ss), std::end(ss));
			seed(ss.front());
		}

		auto operator()() -> result_type { return m_engine.Rndm() * max(); };
		void discard(unsigned long long n) {
			nxx::repeat_n(n, [this]() { (*this)(); });
		}

		friend auto operator==(engine const& lhs, engine const& rhs) -> bool {
			return lhs.m_engine.GetSeed() == rhs.m_engine.GetSeed();
		}

		friend auto operator!=(engine const& lhs, engine const& rhs) -> bool { return !(lhs == rhs); }

		// FIXME: find a better way of streaming; this doesn't work with any ostream
		friend auto operator<<(std::ostream& os, engine const& e) -> decltype(os)& {
			auto const tmp = std::tmpnam(nullptr);
			TFile file{tmp, "recreate"};
			e.m_engine.Write();
			return os << std::ifstream{tmp, std::ios_base::binary}.rdbuf();
		}

		// FIXME: find a better way of streaming; this doesn't work with any istream
		friend auto operator>>(std::istream& is, engine& e) -> decltype(is)& {
			auto const tmp = std::tmpnam(nullptr);
			std::ofstream fs{tmp, std::ios_base::binary | std::ios_base::binary};
			;
			fs << is.rdbuf();
			TFile file{tmp, "read"};
			auto key = dynamic_cast<TKey*>(file.GetListOfKeys()->First());
			auto engine = dynamic_cast<engine_type*>(key->ReadObj());
			e.m_engine = *engine;
			return is;
		}

	private:
		engine_type m_engine{};
	};

}  // namespace root::random

#endif  // #ifndef ROOT__RANDOM_HXX
