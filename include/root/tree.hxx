#ifndef ROOT__TREE_HXX
#define ROOT__TREE_HXX

#include <cstddef>
#include <iterator>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include <TLeaf.h>
#include <TTree.h>

#include "root/file.hxx"

namespace root::io {

	class tree {
	public:
		class entry;
		using value_type = entry;

	private:
		template <typename T,
		          typename = std::enable_if_t<std::is_same_v<std::remove_const_t<T>, tree::value_type>>>
		class iterator_impl;

	public:
		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;
		using reference = value_type&;
		using const_reference = value_type const&;
		using iterator = iterator_impl<value_type>;
		using const_iterator = iterator_impl<value_type const>;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		class entry {
		public:
			auto index() const { return m_index; }

			auto get_leaf(std::string_view name) const -> void* {
				load_entry();
				auto leaf = m_tree->GetLeaf(name.data());
				if (!leaf)
					return nullptr;
				return leaf->GetValuePointer();
			}

			template <typename T>
			auto get_leaf(std::string_view name) const -> std::optional<T> {
				auto value = get_leaf(name);
				if (!value)
					return {};
				return *static_cast<T*>(value);
			}

			auto get() const {
				auto entry = std::unordered_map<std::string, void*>{};
				auto leaves = m_tree->GetListOfLeaves();
				for (auto obj : *leaves) {
					auto& leaf = dynamic_cast<TLeaf&>(*obj);
					entry.emplace(leaf.GetName(), get_leaf(leaf.GetName()));
				}
				return entry;
			}

			template <typename... Strings>
			auto get(Strings&&... names) const
			    -> std::enable_if_t<std::conjunction_v<std::is_convertible<Strings, std::string>...>,
			                        std::unordered_map<std::string, void*>> {
				auto entry = std::unordered_map<std::string, void*>{};
				std::string name{};
				((name = std::string{names}, entry.emplace(name, get_leaf(name.c_str()))), ...);
				return entry;
			}

			template <typename... Ts, typename... Strings>
			auto get(Strings&&... names) const -> std::enable_if_t<
			    std::conjunction_v<std::bool_constant<sizeof...(Strings) == sizeof...(Ts)>,
			                       std::is_convertible<Strings, std::string_view>...>,
			    std::tuple<std::optional<Ts>...>> {
				return {get_leaf<Ts>(names)...};
			}

			friend bool operator==(entry const& lhs, entry const& rhs) {
				return (lhs.m_tree == rhs.m_tree) && (lhs.m_index == rhs.m_index);
			}
			friend bool operator!=(entry const& lhs, entry const& rhs) { return !(lhs == rhs); }

		private:
			friend class tree;

			template <typename, typename>
			friend class iterator_impl;

			entry() = default;

			explicit entry(TTree* tree, tree::size_type index = 0)
			    : m_tree{tree}
			    , m_index{index} {};

			void load_entry() const {
				if (m_tree->GetReadEntry() != m_index)
					m_tree->GetEntry(m_index);
			}

			TTree* m_tree{};
			tree::size_type m_index{};
		};


		explicit tree(std::unique_ptr<TTree> tree)
		    : m_tree{std::move(tree)} {}

		auto operator*() const -> TTree& { return *m_tree; };
		auto operator-> () const -> TTree* { return &**this; };

		auto size() const -> size_type { return m_tree->GetEntries(); }

		auto begin() { return iterator{m_tree.get(), 0}; }
		auto begin() const { return const_iterator{m_tree.get(), 0}; }
		auto cbegin() const { return begin(); }
		auto end() { return iterator{m_tree.get(), size()}; }
		auto end() const { return const_iterator{m_tree.get(), size()}; }
		auto cend() const { return end(); }

		auto at(size_type pos) const { return entry{m_tree.get(), pos}; }


	private:
		template <typename T>
		class iterator_impl<T> {
		public:
			using iterator_category = std::random_access_iterator_tag;
			using value_type = tree::value_type;
			using difference_type = tree::difference_type;
			using pointer = T*;
			using reference = tree::const_reference;

			iterator_impl() = default;

			auto operator*() const -> reference { return m_entry; }
			auto operator-> () const -> pointer { return &**this; }

			auto operator+=(difference_type n) -> iterator_impl& {
				m_entry.m_index += n;
				return *this;
			}

			auto operator+(difference_type n) -> iterator_impl {
				auto tmp = *this;
				return tmp += n;
			}

			auto operator++() -> iterator_impl& { return *this += 1; }

			auto operator++(int) -> iterator_impl {
				auto tmp = *this;
				++*this;
				return tmp;
			}

			auto operator-=(difference_type n) -> iterator_impl& { return *this += -n; }

			auto operator-(difference_type n) -> iterator_impl { return *this + -n; }

			auto operator--() -> iterator_impl& { return *this -= 1; }

			auto operator--(int) -> iterator_impl {
				auto tmp = *this;
				--*this;
				return tmp;
			}

			auto operator[](difference_type n) -> reference { return *(*this + n); }

			friend auto operator-(iterator_impl const& lhs, iterator_impl const& rhs) -> difference_type {
				return lhs->index() - rhs->index();
			}

			friend bool operator==(iterator_impl const& lhs, iterator_impl const& rhs) {
				return *lhs == *rhs;
			}
			friend bool operator!=(iterator_impl const& lhs, iterator_impl const& rhs) {
				return !(lhs == rhs);
			}

			friend bool operator<(iterator_impl const& lhs, iterator_impl const& rhs) {
				return lhs->index() < rhs->index();
			}
			friend bool operator>(iterator_impl const& lhs, iterator_impl const& rhs) { return rhs < lhs; }
			friend bool operator<=(iterator_impl const& lhs, iterator_impl const& rhs) {
				return !(lhs > rhs);
			}
			friend bool operator>=(iterator_impl const& lhs, iterator_impl const& rhs) {
				return !(lhs < rhs);
			}

		private:
			friend class tree;

			explicit iterator_impl(TTree* tree, size_type index = 0)
			    : m_entry{tree, index} {}

			entry m_entry{};
		};

		std::unique_ptr<TTree> m_tree{};
	};


	template <>
	struct get_from_t<tree> {
		template <typename From>
		auto operator()(gettable_from<From> const& from, std::string_view name) const
		    -> traits::get_from_result_t<tree> {
			auto ptr = from.template get<TTree*>(name);
			if (!ptr)
				return {};
			return tree{std::move(ptr)};
		}
	};

	template <>
	struct write_to_t<tree> {
		template <typename To>
		void operator()(writable_to<To>& to, tree const& tree, std::string_view name) const {
			to.write(*tree, name);
		}

		template <typename To>
		void operator()(writable_to<To>& to, tree const& tree) const {
			to.write(*tree);
		}
	};

}  // namespace root::io

#endif  // ifndef ROOT__TREE_HXX
