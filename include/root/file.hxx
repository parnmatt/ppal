#ifndef ROOT__FILE_HXX
#define ROOT__FILE_HXX

#include <filesystem>
#include <functional>
#include <optional>
#include <string>
#include <string_view>
#include <type_traits>
#include <typeinfo>
#include <utility>

#include <TDirectory.h>
#include <TFile.h>
#include <TMemFile.h>
#include <TObject.h>
#include <TParameter.h>

#include <nxx/bitmask.hxx>
#include <nxx/memory.hxx>
#include <nxx/type_traits.hxx>

#include "root/types.hxx"

namespace root::io {
	enum class openmode : unsigned int {
		in = 0b001,
		out = 0b010,
		app = 0b110,
	};
}

template <>
struct nxx::bitmask::enable<root::io::openmode> : std::true_type {};


namespace root::io {
	class toggle_current_dir {
	public:
		explicit toggle_current_dir(TDirectory& to) { to.cd(); }
		explicit toggle_current_dir(TDirectory* to) { to->cd(); }
		~toggle_current_dir() { from->cd(); }

	private:
		TDirectory* from{gDirectory};
	};

	namespace traits {
		template <typename>
		struct underlying_gettable;

		template <typename File>
		using underlying_gettable_t = typename underlying_gettable<File>::type;


		template <typename T>
		struct get_from_result : std::enable_if_t<std::is_object_v<T>, nxx::empty_base<get_from_result<T>>> {
			using type = std::optional<T>;
		};

		template <typename T>
		using get_from_result_t = typename get_from_result<T>::type;

		template <typename T>
		struct get_from_result<T*>
		    : std::enable_if_t<std::is_object_v<T>, nxx::empty_base<get_from_result<T*>>> {
			using type = std::unique_ptr<T>;
		};

	}  // namespace traits

	template <typename From>
	struct gettable_from {
		using gettable_t = traits::underlying_gettable_t<From>;

		auto operator*() const -> gettable_t const& { return static_cast<From const&>(*this).operator*(); }
		auto operator-> () const -> gettable_t const* { return static_cast<From const&>(*this).operator->(); }

		template <typename T>
		auto get(std::string_view name) const -> traits::get_from_result_t<T> {
			return static_cast<From const&>(*this).template get<T>(name);
		}
	};

	template <typename>
	struct get_from_t;

	namespace detail {

		auto inline get_from(TDirectory const& from, std::string_view name) {
			// TDirectory::Get should be const, but isn't
			// To ensure const-correctness everywhere else, this cast is use here
			// The underlying TDirectory objects are non-const, thus this cast is valid
			return std::unique_ptr<TObject>{const_cast<TDirectory&>(from).Get(name.data())};
		}

		template <typename T>
		auto inline get_value(std::unique_ptr<TObject> ptr) {
			if constexpr (std::is_base_of_v<TObject, T>)
				return dynamic_cast<T&>(*ptr);
			else
				return dynamic_cast<TParameter<T>&>(*ptr).GetVal();
		}

		template <typename T>
		auto inline get_ptr(std::unique_ptr<TObject> ptr) {
			if constexpr (std::is_base_of_v<TObject, T>)
				return nxx::memory::dynamic_pointer_cast_via_ref<T>(std::move(ptr));
			else
				return std::make_unique<T>(get_value<T>(std::move(ptr)));
		}

	}  // namespace detail

	template <typename T>
	auto inline get_from(TDirectory const& from, std::string_view name) -> traits::get_from_result_t<T> {

		auto ptr = detail::get_from(from, name);
		if (!ptr)
			return {};

		if constexpr (nxx::traits::is_value_v<T>)
			return detail::get_value<T>(std::move(ptr));
		else if constexpr (nxx::traits::is_value_pointer_v<T>)
			return detail::get_ptr<std::remove_pointer_t<T>>(std::move(ptr));
		else
			throw std::bad_cast{};
	}

	template <typename T, typename From>
	auto inline get_from(gettable_from<From> const& from, std::string_view name)
	    -> traits::get_from_result_t<T> {
		return get_from_t<T>{}(from, name);
	}

	template <typename T>
	struct get_from_t {
		template <typename From>
		auto operator()(gettable_from<From> const& from, std::string_view name) const
		    -> traits::get_from_result_t<T> {
			return io::get_from<T>(*from, name);
		}
	};


	namespace traits {
		template <typename>
		struct underlying_writable;

		template <typename File>
		using underlying_writable_t = typename underlying_writable<File>::type;

	}  // namespace traits

	template <typename To>
	struct writable_to {
		using writable_t = traits::underlying_writable_t<To>;

		auto operator*() -> writable_t& { return static_cast<To&>(*this).operator*(); }
		auto operator-> () -> writable_t* { return static_cast<To&>(*this).operator->(); }

		template <typename T>
		void write(T&& obj) {
			static_cast<To&>(*this).write(std::forward<T>(obj));
		}

		template <typename T>
		void write(T&& obj, std::string_view name) {
			static_cast<To&>(*this).write(std::forward<T>(obj), name);
		}
	};

	template <typename>
	struct write_to_t;

	void inline write_to(TDirectory& to, TObject const& obj, std::string_view name) {
		toggle_current_dir toggle{to};
		obj.Write(name.data());
	}

	template <typename To>
	void inline write_to(writable_to<To>& to, TObject const& obj, std::string_view name) {
		io::write_to(*to, obj, name);
	}

	template <typename To>
	void inline write_to(writable_to<To>& to, TNamed const& obj) {
		io::write_to(to, obj, {});
	}

	template <typename T, typename To>
	auto inline write_to(writable_to<To>& to, T const& obj, std::string_view name)
	    -> std::enable_if_t<std::negation_v<std::is_base_of<TObject, T>>, void> {
		write_to_t<T>{}(to, obj, name);
	}

	template <typename T, typename To>
	auto inline write_to(writable_to<To>& to, T const& obj)
	    -> std::enable_if_t<std::negation_v<std::is_base_of<TNamed, T>>, void> {
		write_to_t<T>{}(to, obj);
	}

	template <typename T, typename To>
	auto inline operator<<(writable_to<To>& to, T const& obj) -> writable_to<To>& {
		io::write_to(to, obj);
		return to;
	}

	template <typename T>
	struct write_to_t {
		template <typename To>
		void operator()(writable_to<To>& to, T const& obj, std::string_view name) const {
			io::write_to(to, TParameter<T>{name.data(), obj}, name);
		}
	};


	template <openmode = openmode::in | openmode::out, typename = TFile>
	struct file;

	namespace detail {
		template <typename>
		struct ifile;

		template <typename>
		struct ofile;

	}  // namespace detail

	namespace traits {
		template <typename>
		struct underlying_tfile;

		template <typename File>
		using underlying_tfile_t = typename underlying_tfile<File>::type;

		template <openmode Mode, typename ROOTFile>
		struct underlying_tfile<file<Mode, ROOTFile>> {
			using type = ROOTFile;
		};


		template <openmode Mode, typename ROOTFile>
		struct underlying_gettable<file<Mode, ROOTFile>>
		    : std::enable_if_t<nxx::bitmask::test(Mode, openmode::in),
		                       underlying_tfile<file<Mode, ROOTFile>>> {};

		template <typename File>
		struct underlying_gettable<detail::ifile<File>> : underlying_tfile<File> {};


		template <openmode Mode, typename ROOTFile>
		struct underlying_writable<file<Mode, ROOTFile>>
		    : std::enable_if_t<nxx::bitmask::test(Mode, openmode::out),
		                       underlying_tfile<file<Mode, ROOTFile>>> {};

		template <typename File>
		struct underlying_writable<detail::ofile<File>> : underlying_tfile<File> {};

	}  // namespace traits

	namespace detail {

		template <typename File>
		struct ifile : gettable_from<ifile<File>> {
			using gettable_t = traits::underlying_gettable_t<ifile>;

			auto operator*() const -> gettable_t const& {
				return static_cast<File const&>(*this).operator*();
			}
			auto operator-> () const -> gettable_t const* {
				return static_cast<File const&>(*this).operator->();
			}

			template <typename T>
			auto get(std::string_view name) const -> traits::get_from_result_t<T> {
				return io::get_from<T>(*this, name);
			}
		};

		template <typename File>
		struct ofile : writable_to<ofile<File>> {
			using writable_t = traits::underlying_writable_t<ofile>;

			auto operator*() -> writable_t& { return static_cast<File&>(*this).operator*(); }
			auto operator-> () -> writable_t* { return static_cast<File&>(*this).operator->(); }

			template <typename T>
			void write(T const& obj) {
				io::write_to(*this, obj);
			}

			template <typename T>
			void write(T const& obj, std::string_view name) {
				io::write_to(*this, obj, name);
			}
		};

	}  // namespace detail


	template <openmode Mode, typename ROOTFile>
	class file
	    : std::enable_if_t<std::is_base_of_v<TFile, ROOTFile>, nxx::empty_base<file<Mode, ROOTFile>>>
	    , public nxx::optional_base<nxx::bitmask::test(Mode, openmode::in),
	                                detail::ifile<file<Mode, ROOTFile>>>
	    , public nxx::optional_base<nxx::bitmask::test(Mode, openmode::out),
	                                detail::ofile<file<Mode, ROOTFile>>> {

	public:
		using tfile_t = traits::underlying_tfile_t<file>;

		file() = default;
		explicit file(std::filesystem::path name) noexcept
		    : m_file{std::in_place, name.c_str(), tfile_opt(Mode)} {
			is_valid();
		}

		bool is_valid() const noexcept {
			if (!m_file || m_file->IsZombie() || !m_file->IsOpen()) {
				m_file = std::nullopt;
				return false;
			}

			return true;
		}

		operator bool() const noexcept { return is_valid(); }

		bool is_readable() const noexcept { return nxx::bitmask::test(Mode, openmode::in) && is_valid(); }
		bool is_writable() const noexcept { return nxx::bitmask::test(Mode, openmode::out) && is_valid(); }

		void close(std::optional<root::option_t*> option = {}) noexcept {
			if (!m_file)
				return;

			if (option)
				m_file->Close(*option);
			else
				m_file->Close();

			m_file = std::nullopt;
		}

		auto operator*() -> tfile_t& { return m_file.operator*(); };
		auto operator*() const -> tfile_t const& { return m_file.operator*(); };

		auto operator-> () -> tfile_t* { return m_file.operator->(); };
		auto operator-> () const -> tfile_t const* { return m_file.operator->(); };

		auto name() const -> std::filesystem::path { return m_file->GetName(); }

	private:
		auto constexpr tfile_opt(openmode mode) -> root::option_t* {
			nxx::bitmask::unset(mode, openmode::in);
			switch (mode) {
			case openmode::out:
				return "recreate";
			case openmode::app:
				return "update";
			default:
				return "read";
			}
		}

		std::optional<tfile_t> mutable m_file{};
	};

	using ifile = file<openmode::in, TFile>;
	using ofile = file<openmode::out, TFile>;
	using afile = file<openmode::app, TFile>;
	using mem_file = file<openmode::in | openmode::out, TMemFile>;

}  // namespace root::io

#endif  // ifndef ROOT__FILE_HXX
