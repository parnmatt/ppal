#ifndef ROOT__COLORS_HXX
#define ROOT__COLORS_HXX

#include <Rtypes.h>

namespace root::color {

	enum color {
		white = kWhite,
		black = kBlack,
		gray = kGray,
		red = kRed,
		green = kGreen,
		blue = kBlue,
		yellow = kYellow,
		magenta = kMagenta,
		cyan = kCyan,
		orange = kOrange,
		spring = kSpring,
		teal = kTeal,
		azure = kAzure,
		violet = kViolet,
		pink = kPink,
	};

}  // namespace root::color

#endif  // #ifndef ROOT__COLORS_HXX
