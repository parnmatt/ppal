#ifndef ROOT__TYPES_HXX
#define ROOT__TYPES_HXX

#include <Rtypes.h>

namespace root {

	using bool_t = Bool_t;
	using byte_t = Byte_t;
	using char_t = Char_t;
	using uchar_t = UChar_t;
	using short_t = Short_t;
	using ushort_t = UShort_t;
	using int_t = Int_t;
	using uint_t = UInt_t;
	using long_t = Long_t;
	using ulong_t = ULong_t;
	using long64_t = Long64_t;
	using ulong64_t = ULong64_t;
	using float_t = Float_t;
	using float16_t = Float16_t;
	using real_t = Real_t;
	using double_t = Double_t;
	using double32_t = Double32_t;
	using long_double_t = LongDouble_t;

	using seek_t = Seek_t;
	using text_t = Text_t;
	using version_t = Version_t;
	using option_t = Option_t;
	using axis_t = Axis_t;
	using stat_t = Stat_t;
	using font_t = Font_t;
	using style_t = Style_t;
	using marker_t = Marker_t;
	using width_t = Width_t;
	using color_t = Color_t;
	using screen_coord_t = SCoord_t;
	using pad_coord_t = Coord_t;
	using angle_t = Angle_t;
	using size_t = Size_t;

}  // namespace root

#endif  // #ifndef ROOT__TYPES_HXX
