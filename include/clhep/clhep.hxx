#ifndef CLHEP__CLHEP_HXX
#define CLHEP__CLHEP_HXX

#include <iostream>
#include <limits>
#include <sstream>
#include <type_traits>
#include <vector>

#include <CLHEP/Random/DualRand.h>
#include <CLHEP/Random/JamesRandom.h>
#include <CLHEP/Random/MTwistEngine.h>
#include <CLHEP/Random/NonRandomEngine.h>
#include <CLHEP/Random/RandomEngine.h>
#include <CLHEP/Random/RanecuEngine.h>
#include <CLHEP/Random/Ranlux64Engine.h>
#include <CLHEP/Random/RanluxEngine.h>
#include <CLHEP/Random/RanshiEngine.h>

#include <CLHEP/Units/PhysicalConstants.h>

#include <nxx/algorithm.hxx>
#include <nxx/random.hxx>
#include <nxx/utility.hxx>

namespace clhep {
	namespace random {

		using engine_seed_t = unsigned int;
		using engine_seed_param_t = int;
		auto constexpr static hep_random_engine_default_seed = engine_seed_t{19780503};
		auto constexpr static engine_unused_param = std::numeric_limits<engine_seed_param_t>::max();


		namespace traits {

			template <typename Engine, typename = void>
			struct is_engine : std::false_type {};

			template <typename Engine>
			struct is_engine<Engine, typename std::enable_if<
			                             std::is_base_of<CLHEP::HepRandomEngine, Engine>::value
			                             && !std::is_base_of<CLHEP::NonRandomEngine, Engine>::value>::type>
			    : std::true_type {};

			template <typename S, typename Engine, typename = void>
			struct is_engine_seed_sequence : std::false_type {};

			template <typename S, typename Engine>
			struct is_engine_seed_sequence<
			    S, Engine,
			    typename std::enable_if<
			        nxx::random::traits::is_random_number_engine_seed_sequence<S, Engine>::value
			        && !std::is_same<typename std::remove_cv<S>::type,
			                         typename Engine::engine_type>::value>::type> : std::true_type {};

		}  // namespace traits


		template <typename Engine, engine_seed_t DefaultSeed = hep_random_engine_default_seed,
		          engine_seed_param_t DefaultSeedParam = engine_unused_param,
		          engine_seed_param_t DefaultSeedSeqParam = DefaultSeedParam,
		          typename = typename std::enable_if<traits::is_engine<Engine>::value>::type>
		class engine {
		public:
			using engine_type = Engine;
			using result_type = engine_seed_t;
			auto constexpr static default_seed = DefaultSeed;
			auto constexpr static default_seed_param = DefaultSeedParam;
			auto constexpr static default_seed_seq_param = DefaultSeedSeqParam;

			engine() { seed(); }

			template <engine_seed_t S, engine_seed_param_t SP, engine_seed_param_t SSP>
			explicit engine(engine<engine_type, S, SP, SSP> const& engine) {
				std::stringstream ss;
				ss << engine;
				ss >> *this;
			}

			explicit engine(engine_type const& engine) {
				std::stringstream ss;
				ss << engine;
				ss >> *m_engine;
			}

			explicit engine(CLHEP::HepRandomEngine const& engine) {
				std::stringstream ss;
				ss << dynamic_cast<engine_type const&>(engine);
				ss >> *m_engine;
			}

			explicit engine(engine_type* engine)
			    : m_engine{engine} {}
			explicit engine(CLHEP::HepRandomEngine* engine)
			    : m_engine{&dynamic_cast<engine_type&>(*engine)} {}
			explicit engine(result_type seed) { this->seed(seed); }

			template <typename SeedSeq, typename = typename std::enable_if<
			                                traits::is_engine_seed_sequence<SeedSeq, engine>::value>::type>
			explicit engine(SeedSeq& seeds) {
				seed(seeds);
			}


			auto constexpr min() -> result_type const { return 1; }
			auto constexpr max() -> result_type const { return std::numeric_limits<result_type>::max(); }

			void seed() { m_engine->setSeed(default_seed, default_seed_param); }
			void seed(result_type seed, engine_seed_param_t param = default_seed_param) {
				m_engine->setSeed(seed, param);
			}

			template <typename SeedSeq, typename = typename std::enable_if<
			                                traits::is_engine_seed_sequence<SeedSeq, engine>::value>::type>
			void seed(SeedSeq& seeds, engine_seed_param_t param = default_seed_seq_param) {
				auto ss = std::vector<long>{};
				ss.reserve(seeds.size());
				seeds.param(std::back_inserter(ss));
				m_engine->setSeeds(ss.data(), param);
			}

			auto operator()() -> result_type { return *m_engine; }
			void discard(unsigned long long n) {
				nxx::repeat_n(n, [this]() { (*this)(); });
			}

			friend auto operator<<(std::ostream& os, engine const& e) -> decltype(os)& {
				return os << *e.m_engine;
			}
			friend auto operator>>(std::istream& is, engine& e) -> decltype(is)& { return is >> *e.m_engine; }

		private:
			nxx::fake_ptr<engine_type> m_engine{engine_type{default_seed}};
		};

		template <typename E, engine_seed_t Sl, engine_seed_param_t SPl, engine_seed_param_t SSPl,
		          engine_seed_t Sr, engine_seed_param_t SPr, engine_seed_param_t SSPr>
		auto operator==(engine<E, Sl, SPl, SSPl> const& lhs, engine<E, Sr, SPr, SSPr> const& rhs) -> bool {
			std::stringstream ls, rs;
			ls << lhs;
			rs << rhs;
			return ls.str() == rs.str();
		}

		template <typename E, engine_seed_t Sl, engine_seed_param_t SPl, engine_seed_param_t SSPl,
		          engine_seed_t Sr, engine_seed_param_t SPr, engine_seed_param_t SSPr>
		auto operator!=(engine<E, Sl, SPl, SSPl> const& lhs, engine<E, Sr, SPr, SSPr> const& rhs) -> bool {
			return !(lhs == rhs);
		}


		template <engine_seed_t DefaultSeed>
		using dual_rand_engine = engine<CLHEP::DualRand, DefaultSeed>;
		using dual_rand = dual_rand_engine<1234567>;

		template <engine_seed_t DefaultSeed>
		using hep_james_random = engine<CLHEP::HepJamesRandom, DefaultSeed>;
		template <engine_seed_t DefaultSeed>
		using james_engine = hep_james_random<DefaultSeed>;
		using james = james_engine<hep_random_engine_default_seed>;

		template <engine_seed_t DefaultSeed, engine_seed_param_t K>
		using mtwist_engine = engine<CLHEP::MTwistEngine, DefaultSeed, K>;
		template <engine_seed_t DefaultSeed, engine_seed_param_t K>
		using mt19937_engine = mtwist_engine<DefaultSeed, K>;
		using mt19937 = mt19937_engine<4357, 0>;

		template <engine_seed_t DefaultSeed, engine_seed_param_t Dum, engine_seed_param_t Index>
		using ranecu_engine = engine<CLHEP::RanecuEngine, DefaultSeed, Dum, Index>;
		using ranecu = ranecu_engine<hep_random_engine_default_seed, 0, 1>;

		// unfortunate naming: CLHEP names due to 64-calculation bits, STL names due to 48-random bits.
		template <engine_seed_t DefaultSeed, engine_seed_param_t Lux>
		using ranlux64_engine = engine<CLHEP::Ranlux64Engine, DefaultSeed, Lux>;
		template <engine_seed_t DefaultSeed, engine_seed_param_t Lux>
		using ranlux48_engine = ranlux64_engine<DefaultSeed, Lux>;
		using ranlux48 = ranlux48_engine<32767, 1>;

		// unfortunate naming: CLHEP names due to algorithm, STL names due to 24-random bits.
		template <engine_seed_t DefaultSeed, engine_seed_param_t Lux>
		using ranlux_engine = engine<CLHEP::RanluxEngine, DefaultSeed, Lux>;
		template <engine_seed_t DefaultSeed, engine_seed_param_t Lux>
		using ranlux24_engine = ranlux_engine<DefaultSeed, Lux>;
		using ranlux24 = ranlux24_engine<32767, 3>;

		template <engine_seed_t DefaultSeed>
		using ranshi_engine = engine<CLHEP::RanshiEngine, DefaultSeed>;
		using ranshi = ranshi_engine<hep_random_engine_default_seed>;

	}  // namespace random

	namespace constant {

		using CLHEP::halfpi;
		using CLHEP::pi;
		using CLHEP::pi2;
		using CLHEP::twopi;

		using CLHEP::Avogadro;

		using CLHEP::c_light;
		using CLHEP::c_squared;

		using CLHEP::h_Planck;
		using CLHEP::hbar_Planck;
		using CLHEP::hbarc;
		using CLHEP::hbarc_squared;

		using CLHEP::e_squared;
		using CLHEP::electron_charge;

		using CLHEP::amu;
		using CLHEP::amu_c2;
		using CLHEP::electron_mass_c2;
		using CLHEP::neutron_mass_c2;
		using CLHEP::proton_mass_c2;

		using CLHEP::epsilon0;
		using CLHEP::mu0;

		using CLHEP::Bohr_radius;
		using CLHEP::classic_electr_radius;
		using CLHEP::electron_Compton_length;
		using CLHEP::elm_coupling;
		using CLHEP::fine_structure_const;

		using CLHEP::alpha_rcl2;

		using CLHEP::twopi_mc2_rcl2;

		using CLHEP::k_Boltzmann;

		using CLHEP::kGasThreshold;
		using CLHEP::STP_Pressure;
		using CLHEP::STP_Temperature;

		using CLHEP::universe_mean_density;

	}  // namespace constant
}  // namespace clhep

#endif  // #ifndef CLHEP__CLHEP_HXX
