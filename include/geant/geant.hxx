#ifndef GEANT__GEANT_HXX
#define GEANT__GEANT_HXX

#include <G4String.hh>
#include <G4Types.hh>
#include <Randomize.hh>

#include "clhep/clhep.hxx"

namespace geant {

	using bool_t = G4bool;
	using int_t = G4int;
	using long_t = G4long;
	using float_t = G4float;
	using double_t = G4double;
	using complex_t = G4complex;
	using string = G4String;

	namespace random {

		using namespace clhep::random;
		auto static default_engine = geant::random::james{G4Random::getTheEngine()};

	}  // namespace random

	namespace constant {
		using namespace clhep::constant;
	}  //  namespace constant

}  // namespace geant

#endif  // #ifndef GEANT__GEANT_HXX
