#ifndef PPAL__STATS_HXX
#define PPAL__STATS_HXX

#include <cassert>
#include <iterator>
#include <numeric>
#include <type_traits>

namespace ppal::stats {

	template <typename T = void>
	class mean_t {
	public:
		template <typename InputIt>
		auto constexpr operator()(InputIt first, InputIt last) const noexcept -> decltype(auto) {
			assert(first != last);
			return impl(first, last, typename std::iterator_traits<InputIt>::iterator_category{});
		}

	private:
		template <typename InputIt>
		auto constexpr static impl(InputIt first, InputIt last, std::input_iterator_tag) noexcept {
			using traits_t = std::iterator_traits<InputIt>;
			using counter_t = typename traits_t::difference_type;
			using value_t = std::conditional_t<std::is_same_v<T, void>, typename traits_t::value_type, T>;

			counter_t counter = 1;
			value_t value = *first++;
			while (first != last) {
				value = value + *first++;
				++counter;
			}

			return value / counter;
		}

		template <typename RandAccessIt>
		auto constexpr static impl(RandAccessIt first, RandAccessIt last,
		                           std::random_access_iterator_tag) noexcept {
			using value_t = std::conditional_t<std::is_same_v<T, void>,
			                                   typename std::iterator_traits<RandAccessIt>::value_type, T>;

			auto const n = last - first;
			value_t initial = *first++;
			return std::accumulate(first, last, std::move(initial)) / n;
		}
	};

	template <typename T = void>
	auto constexpr inline mean = mean_t<T>{};


	template <typename T = void, bool Sample = false>
	class variance_t {
	public:
		template <typename ForwardIt>
		auto constexpr operator()(ForwardIt first, ForwardIt last) const -> decltype(auto) {
			assert(first != last);
			if constexpr (Sample)
				return impl(first, last) / (std::distance(first, last) - 1);
			else
				return impl(first, last) / std::distance(first, last);
		}

	protected:
		template <typename ForwardIt>
		auto constexpr static impl(ForwardIt first, ForwardIt last) {
			using value_t = typename std::iterator_traits<ForwardIt>::value_type;

			auto const mean_ = mean<T>(first, last);
			return std::accumulate(first, last, value_t{0}, [&](auto const& sum, auto const& value) {
				auto diff = value - mean_;
				return sum + diff * diff;
			});
		}
	};

	template <typename T = void>
	auto constexpr inline population_variance = variance_t<T, false>{};

	template <typename T = void>
	auto constexpr inline sample_variance = variance_t<T, true>{};

	template <typename T = void>
	auto constexpr inline variance = population_variance<T>;


	template <typename T = void, bool Sample = false>
	struct standard_deviation_t {
		template <typename ForwardIt>
		auto constexpr operator()(ForwardIt first, ForwardIt last) const -> decltype(auto) {
			assert(first != last);
			if constexpr (Sample)
				return std::sqrt(sample_variance<T>(first, last));
			else
				return std::sqrt(population_variance<T>(first, last));
		}
	};

	template <typename T = void>
	auto constexpr inline population_standard_deviation = standard_deviation_t<T, false>{};

	template <typename T = void>
	auto constexpr inline sample_standard_deviation = standard_deviation_t<T, true>{};

	template <typename T = void>
	auto constexpr inline standard_deviation = population_standard_deviation<T>;


	template <typename T = void, bool Estimated = false>
	struct standard_error_t {
		template <typename ForwardIt>
		auto constexpr operator()(ForwardIt first, ForwardIt last) const -> decltype(auto) {
			assert(first != last);
			if constexpr (Estimated)
				return sample_standard_deviation<T>(first, last) / std::distance(first, last);
			else
				return population_standard_deviation<T>(first, last) / std::distance(first, last);
		}
	};

	template <typename T = void>
	auto constexpr inline standard_error = standard_error_t<T, false>{};

	template <typename T = void>
	auto constexpr inline estimated_standard_error = standard_error_t<T, true>{};

}  // namespace ppal::stats

#endif  // ifndef PPAL__STATS_HXX
