#ifndef PPAL__OPTIMIZE_HXX
#define PPAL__OPTIMIZE_HXX

#include <algorithm>
#include <array>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iterator>
#include <limits>
#include <optional>
#include <ratio>
#include <tuple>
#include <type_traits>
#include <utility>

#include <nxx/boolean.hxx>
#include <nxx/expressions.hxx>
#include <nxx/tuple.hxx>
#include <nxx/type_traits.hxx>
#include <nxx/utility.hxx>

namespace ppal::optimize {
	namespace terminator {
		namespace detail {

			template <typename F>
			class named {
			public:
				using callable_type = F;

				constexpr named() = default;

				explicit constexpr named(F&& f)
				    : m_f{std::forward<F>(f)} {}

				template <typename... Args>
				explicit named(Args&&... args)
				    : m_f(std::forward<Args>(args)...) {}

				template <typename... Args>
				auto constexpr operator()(Args&&... args) const -> decltype(auto) {
					return std::invoke(m_f, std::forward<Args>(args)...);
				}

			private:
				F m_f;
			};


			template <typename T>
			struct range_tolerance {
				auto constexpr tolerance() const -> decltype(auto) {
					return static_cast<T const&>(*this).tolerance();
				}

				template <typename It>
				auto constexpr tolerance(It first, It last) const -> decltype(auto) {
					return static_cast<T const&>(*this).tolerance(first, last);
				}

				template <typename It>
				bool constexpr operator()(It first, It last) const {
					return tolerance(first, last) < tolerance();
				}
			};

			template <typename T,
			          std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>,
			                           bool> = std::numeric_limits<std::remove_reference_t<T>>::is_integer>
			class absolute_tolerance {
			public:
				constexpr absolute_tolerance() = default;

				explicit constexpr absolute_tolerance(T&& tol)
				    : m_tol{std::forward<T>(tol)} {
					assert(!(m_tol < T{0}));
				}

				auto constexpr tolerance() const -> T const& { return m_tol; }

			private:
				T m_tol{0};
			};

			template <typename T>
			struct absolute_tolerance<T, false> : absolute_tolerance<T, true> {
				using absolute_tolerance<T, true>::absolute_tolerance;
				constexpr absolute_tolerance()
				    : absolute_tolerance{std::nexttoward(
				        std::sqrt(std::numeric_limits<std::remove_reference_t<T>>::epsilon()), 1.0)} {}
			};


			template <typename T,
			          std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>,
			                           bool> = std::numeric_limits<std::remove_reference_t<T>>::is_integer>
			class relative_tolerance {
			public:
				constexpr relative_tolerance() = default;

				explicit constexpr relative_tolerance(T&& tol)
				    : m_tol{std::forward<T>(tol)} {
					assert(T{0} < m_tol);
				}

				auto constexpr tolerance() const -> T const& { return m_tol; }

			private:
				T m_tol{1};
			};

			template <typename T>
			struct relative_tolerance<T, false> : relative_tolerance<T, true> {
				using relative_tolerance<T, true>::relative_tolerance;
				constexpr relative_tolerance()
				    : relative_tolerance{
				        std::nexttoward(std::numeric_limits<std::remove_reference_t<T>>::epsilon(), 1.0)} {}
			};


		}  // namespace detail


		namespace functions {

			struct absolute_tolerance_t {
				template <typename T, std::enable_if_t<std::negation_v<nxx::traits::is_iterator<T>>, int> = 0>
				auto constexpr operator()(T const& a, T const& b) const -> decltype(auto) {
					return std::abs(b - a);
				}

				template <typename ForwardIt,
				          std::enable_if_t<nxx::traits::is_iterator_v<ForwardIt>, int> = 0>
				auto constexpr operator()(ForwardIt first, ForwardIt last) const -> decltype(auto) {
					assert(first != last);
					auto [best, worst] = std::minmax_element(first, last);
					return (*this)(*best, *worst);
				}
			};

			auto constexpr inline absolute_tolerance = absolute_tolerance_t{};


			struct relative_tolerance_t {
				template <typename T, std::enable_if_t<std::negation_v<nxx::traits::is_iterator<T>>, int> = 0>
				auto constexpr operator()(T const& a, T const& b) const -> decltype(auto) {
					return 2 * absolute_tolerance(a, b) / (std::abs(b) + std::abs(a));
				}

				template <typename ForwardIt,
				          std::enable_if_t<nxx::traits::is_iterator_v<ForwardIt>, int> = 0>
				auto constexpr operator()(ForwardIt first, ForwardIt last) const -> decltype(auto) {
					assert(first != last);
					auto [best, worst] = std::minmax_element(first, last);
					return (*this)(*best, *worst);
				}
			};

			auto constexpr inline relative_tolerance = relative_tolerance_t{};


		}  // namespace functions


		template <typename F>
		struct domain : detail::named<F> {
			using detail::named<F>::named;
		};

		template <typename F>
		domain(F &&)->domain<F>;


		template <typename F>
		struct range : detail::named<F> {
			using detail::named<F>::named;
		};

		template <typename F>
		range(F &&)->range<F>;


		template <typename F>
		struct iterations : detail::named<F> {
			using detail::named<F>::named;
		};

		template <typename F>
		iterations(F &&)->iterations<F>;


		template <typename F>
		struct function_calls : detail::named<F> {
			using detail::named<F>::named;
		};

		template <typename F>
		function_calls(F &&)->function_calls<F>;


		using terminate_t = nxx::boolean::static_true_t;
		auto constexpr terminate = terminate_t{};

		using noop_t = nxx::boolean::static_false_t;
		auto constexpr noop = noop_t{};


		template <typename T, typename F = functions::absolute_tolerance_t>
		class absolute_tolerance
		    : public detail::absolute_tolerance<T>
		    , public detail::range_tolerance<absolute_tolerance<T, F>> {

		public:
			using detail::absolute_tolerance<T>::absolute_tolerance;
			using detail::absolute_tolerance<T>::tolerance;
			using detail::range_tolerance<absolute_tolerance<T, F>>::operator();

			explicit constexpr absolute_tolerance(T&& tol, F&& f)
			    : m_f{std::forward<F>(f)}
			    , detail::absolute_tolerance<T>::absolute_tolerance{std::forward<T>(tol)} {}

			template <typename It>
			auto constexpr tolerance(It first, It last) const -> decltype(auto) {
				assert(first != last);
				return m_f(first, last);
			}

		private:
			F m_f{};
		};

		template <typename T>
		absolute_tolerance(T &&)->absolute_tolerance<T>;

		template <typename T, typename F>
		absolute_tolerance(T&&, F &&)->absolute_tolerance<T, F>;


		template <typename T, typename F = functions::relative_tolerance_t>
		class relative_tolerance
		    : public detail::relative_tolerance<T>
		    , public detail::range_tolerance<relative_tolerance<T, F>> {

		public:
			using detail::relative_tolerance<T>::relative_tolerance;
			using detail::relative_tolerance<T>::tolerance;
			using detail::range_tolerance<relative_tolerance<T, F>>::operator();

			template <typename It>
			auto constexpr tolerance(It first, It last) const -> decltype(auto) {
				assert(first != last);
				return m_f(first, last);
			}

		private:
			F m_f{};
		};

		template <typename T>
		relative_tolerance(T &&)->relative_tolerance<T>;

		template <typename T, typename F>
		relative_tolerance(T&&, F &&)->relative_tolerance<T, F>;

	}  // namespace terminator


	namespace detail {

		struct sum {
			template <typename... Args>
			auto constexpr operator()(Args&&... args) const {
				return (std::forward<Args>(args) + ...);
			}
		};

		template <typename V, std::size_t... Is>
		auto constexpr sum_dimensions_impl(V const& v, std::index_sequence<Is...>) {
			return nxx::expressions::element_wise::vector_expression{sum{}, std::get<Is>(v)...};
		}

		template <std::size_t N, typename V>
		auto constexpr sum_dimensions(V const& v) -> decltype(auto) {
			return sum_dimensions_impl(v, std::make_index_sequence<N>{});
		}

		template <typename T, std::size_t N>
		auto constexpr sum_dimensions(std::array<T, N> const& array) {
			return sum_dimensions<N>(array);
		}


	}  // namespace detail


	template <std::size_t N, typename T, typename F, typename ReflectScale = std::ratio<1>,
	          typename ExpandScale = std::ratio_multiply<ReflectScale, std::ratio<2>>,
	          typename ContractScale = std::ratio_divide<ReflectScale, std::ratio<2>>,
	          typename ShrinkScale = ContractScale>
	class amoeba
	    : std::enable_if_t<
	          std::conjunction_v<std::is_arithmetic<T>, std::ratio_greater<ReflectScale, std::ratio<0>>,
	                             std::ratio_less<std::ratio<0>, ContractScale>,
	                             std::ratio_less<ContractScale, std::ratio<1>>,
	                             std::ratio_greater<ExpandScale, std::ratio<1>>,
	                             std::ratio_greater<ExpandScale, ReflectScale>,
	                             std::ratio_less<std::ratio<0>, ShrinkScale>,
	                             std::ratio_less<ShrinkScale, std::ratio<1>>>,
	          nxx::empty_base<amoeba<N, T, F, ReflectScale, ExpandScale, ContractScale, ShrinkScale>>> {
	private:
		auto constexpr static n_dimensions = N;
		auto constexpr static n_points = n_dimensions + 1;

		using dimension_t = T;
		using point_t = std::array<dimension_t, n_dimensions>;
		using points_t = std::array<point_t, n_points>;

		using func_t = nxx::call_counter<nxx::tuple::apply<F>>;

		using metric_t = std::invoke_result_t<func_t, point_t>;
		using metrices_t = std::array<metric_t, n_points>;

		using index_t = std::common_type_t<typename points_t::size_type, typename metrices_t::size_type>;

	public:
		template <typename InputIt,
		          std::enable_if_t<
		              std::is_convertible_v<typename std::iterator_traits<typename std::iterator_traits<
		                                        InputIt>::value_type::const_iterator>::value_type,
		                                    dimension_t>,
		              int> = 0>
		explicit amoeba(InputIt first, F&& f)
		    : m_f{std::forward<F>(f)} {

			// copy given points
			auto point = std::begin(m_points);
			for (std::size_t i{}; i < n_points; ++i) {
				auto const& given_point = *first++;
				std::copy(std::begin(given_point), std::end(given_point), std::begin(*point++));
			}

			init();
		}


		// {o, o + e0, o + e1, o + e2, ..., o + eN}
		template <
		    typename InputIt1, typename InputIt2,
		    std::enable_if_t<
		        std::is_convertible_v<
		            std::invoke_result_t<std::plus<>, typename std::iterator_traits<InputIt1>::reference,
		                                 typename std::iterator_traits<InputIt2>::reference>,
		            dimension_t>,
		        int> = 0>
		explicit amoeba(InputIt1 first, InputIt2 diff_first, F&& f)
		    : m_f{std::forward<F>(f)} {

			auto point = std::begin(m_points);
			auto& origin = *point++;
			std::copy_n(first, n_dimensions, std::begin(origin));

			while (point != std::end(m_points)) {
				auto const index = std::distance(std::begin(m_points), point) - 1;
				auto dimension = std::begin(*point++);
				std::copy(std::begin(origin), std::end(origin), dimension);
				std::advance(dimension, index);
				*dimension += *diff_first++;
			}

			init();
		}


		template <typename InputIt, typename S,
		          std::enable_if_t<
		              std::conjunction_v<
		                  std::negation<nxx::traits::is_iterator<std::remove_reference_t<S>>>,
		                  std::is_convertible<
		                      std::invoke_result_t<
		                          std::plus<>, typename std::iterator_traits<InputIt>::reference, S const&>,
		                      dimension_t>>,
		              int> = 0>
		explicit amoeba(InputIt first, S&& diff, F&& f)
		    : amoeba{first, nxx::iterator::value{std::forward<S>(diff)}, std::forward<F>(f)} {}


		void constexpr step() {
			auto& metric = step_metric();
			auto better_point_found = reflect_worst();

			if (better_point_found && !(best_metric() < metric))
				// better_point_found  &&  metric <= best_metric
				// better than current best; probe farther
				expand_father();

			else if (!(better_point_found && metric < next_worst_metric()))
				// !better_point_found  ||  metric >= next_worst_metric
				// still worse than next worst; probe closer
				contract_or_shrink();

			reindex();
			++m_n_steps;
		}

		auto constexpr n_steps() const { return m_n_steps; }
		auto constexpr n_calls() const { return m_f.n_calls(); }
		auto constexpr point() const -> point_t const& { return best_point(); }
		auto constexpr metric() const -> metric_t const& { return best_metric(); }

		template <typename Domain = terminator::noop_t, typename Range = terminator::noop_t,
		          typename Iterations = terminator::noop_t, typename Calls = terminator::noop_t,
		          std::enable_if_t<
		              std::conjunction_v<std::is_invocable_r<bool, Domain, typename points_t::const_iterator,
		                                                     typename points_t::const_iterator>,
		                                 std::is_invocable_r<bool, Range, typename metrices_t::const_iterator,
		                                                     typename metrices_t::const_iterator>,
		                                 std::is_invocable_r<bool, Iterations, std::size_t>,
		                                 std::is_invocable_r<bool, Calls, std::size_t>>,
		              int> = 0>
		auto constexpr operator()(terminator::domain<Domain> domain = {}, terminator::range<Range> range = {},
		                          terminator::iterations<Iterations> n_iterations = {},
		                          terminator::function_calls<Calls> n_function_calls = {})
		    -> std::pair<point_t const&, metric_t const&> {

			auto minimize = [&] {
				auto const terminate = domain(std::cbegin(m_points), std::cend(m_points))
				                       || range(std::cbegin(m_metrices), std::cend(m_metrices))
				                       || n_iterations(n_steps()) || n_function_calls(n_calls());
				return !terminate;
			};

			while (minimize())
				step();

			return {point(), metric()};
		}


	private:
		// constructor helper
		void constexpr init() {
			// calculate initial metrices
			std::transform(std::begin(m_points), std::end(m_points), std::begin(m_metrices), std::ref(m_f));

			// find worst, next worst, and best metrices
			reindex();

			// marginalize each dimension
			sum_points();
		}


		void constexpr reindex() {
			// find worst, next worst, and best metrices
			auto indices = std::array<index_t, n_points>{};
			std::iota(std::begin(indices), std::end(indices), index_t{});

			auto first = std::begin(indices);
			auto last = std::end(indices);

			auto worst = std::prev(last);
			auto next_worst = std::prev(worst);
			auto best = first;

			auto comparitor = [&](index_t lhs, index_t rhs) { return m_metrices[lhs] < m_metrices[rhs]; };
			std::nth_element(first, next_worst, last, comparitor);
			std::nth_element(first, best, next_worst, comparitor);

			m_worst = *worst;
			m_next_worst = *next_worst;
			m_best = *best;

			m_step = m_worst;
		}

		void constexpr sum_points() { m_sum = detail::sum_dimensions(m_points); }

		auto constexpr worst_point() -> point_t& { return m_points[m_worst]; }
		auto constexpr worst_point() const -> point_t const& { return m_points[m_worst]; }

		auto constexpr next_worst_point() -> point_t& { return m_points[m_next_worst]; }
		auto constexpr next_worst_point() const -> point_t const& { return m_points[m_next_worst]; }

		auto constexpr best_point() -> point_t& { return m_points[m_best]; }
		auto constexpr best_point() const -> point_t const& { return m_points[m_best]; }

		auto constexpr step_point() -> point_t& { return m_points[m_step]; }
		auto constexpr step_point() const -> point_t const& { return m_points[m_step]; }

		auto constexpr worst_metric() -> metric_t& { return m_metrices[m_worst]; }
		auto constexpr worst_metric() const -> metric_t const& { return m_metrices[m_worst]; }

		auto constexpr next_worst_metric() -> metric_t& { return m_metrices[m_next_worst]; }
		auto constexpr next_worst_metric() const -> metric_t const& { return m_metrices[m_next_worst]; }

		auto constexpr best_metric() -> metric_t& { return m_metrices[m_best]; }
		auto constexpr best_metric() const -> metric_t const& { return m_metrices[m_best]; }

		auto constexpr step_metric() -> metric_t& { return m_metrices[m_step]; }
		auto constexpr step_metric() const -> metric_t const& { return m_metrices[m_step]; }


		auto constexpr centroid(point_t const& point) const -> point_t {
			namespace expr = nxx::expressions;
			return (expr::vector{m_sum} - expr::vector{point}) / expr::scalar{n_points - 1};
		}

		template <typename Ratio>
		auto constexpr static scale(point_t const& point, point_t const& origin) -> point_t {
			namespace expr = nxx::expressions;
			using Compliment = std::ratio_subtract<std::ratio<1>, Ratio>;

			return expr::vector{point} * expr::scalar{Ratio::num} / expr::scalar{Ratio::den}
			       + expr::vector{origin} * expr::scalar{Compliment::num} / expr::scalar{Compliment::den};
		}

		auto constexpr static reflect(point_t const& point, point_t const& origin) {
			return scale<std::ratio_subtract<std::ratio<0>, ReflectScale>>(point, origin);
		}
		auto constexpr reflect(point_t const& point) const { return reflect(point, centroid(point)); }


		auto constexpr static expand(point_t const& point, point_t const& origin) {
			return scale<ExpandScale>(point, origin);
		}
		auto constexpr expand(point_t const& point) const { return expand(point, centroid(point)); }


		auto constexpr static contract(point_t const& point, point_t const& origin) {
			return scale<ContractScale>(point, origin);
		}
		auto constexpr contract(point_t const& point) const { return contract(point, centroid(point)); }


		auto constexpr static shrink(points_t const& points, point_t const& origin) {
			points_t new_points{};
			std::transform(std::begin(points), std::end(points), std::begin(new_points),
			               [&](point_t const& point) { return scale<ShrinkScale>(point, origin); });
			return new_points;
		}
		auto constexpr shrink_around(point_t const& origin) const { return shrink(m_points, origin); }

		bool constexpr replace_if_better(point_t new_point, index_t index) {
			namespace expr = nxx::expressions;

			auto& metric = m_metrices[index];
			auto new_metric = m_f(new_point);

			if (!(new_metric < metric))
				return false;

			auto& point = m_points[index];

			m_sum = expr::vector{m_sum} - expr::vector{point} + expr::vector{new_point};
			point = std::move(new_point);
			metric = std::move(new_metric);
			return true;
		}

		bool constexpr reflect_worst() {
			// probe by reflecting worst point through face of simplex
			return replace_if_better(reflect(worst_point()), m_worst);
		}

		bool constexpr expand_father() {
			// direction was better; probe farther
			return replace_if_better(expand(step_point()), m_step);
		}

		bool constexpr contract_or_shrink() {
			// probed too far, probe less
			auto better_point_found = replace_if_better(contract(step_point()), m_step);

			if (better_point_found)
				return true;

			// cannot remove worst point, shrink all dimensions
			m_points = shrink_around(best_point());
			sum_points();
			return false;
		}

		void constexpr continue_probing() {
			auto& point = worst_point();
			auto& metric = worst_metric();

			if (!(best_metric() < metric)) {  // metric <= best_metric
				// better than current best; probe farther
				replace_if_better(expand(point), m_worst);

			} else if (!(metric < next_worst_metric())) {  // metric >= next_worst_metric
				// worse than next worst; probe closer
				auto better_point_found = replace_if_better(contract(point), m_worst);

				// cannot remove worst point
				if (!better_point_found) {
					m_points = shrink_around(best_point());
					sum_points();
				}
			}
		}

		func_t m_f;

		points_t m_points{};
		metrices_t m_metrices{};

		point_t m_sum;

		index_t m_worst;
		index_t m_next_worst;
		index_t m_best;

		index_t m_step;

		std::size_t m_n_steps{};
	};

	template <std::size_t N, typename T, typename InputIt, typename F, typename ReflectScale = std::ratio<1>,
	          typename ExpandScale = std::ratio_multiply<ReflectScale, std::ratio<2>>,
	          typename ContractScale = std::ratio_divide<ReflectScale, std::ratio<2>>,
	          typename ShrinkScale = ContractScale>
	auto constexpr make_amoeba(InputIt first, F&& f) {
		return amoeba<N, T, F, ReflectScale, ExpandScale, ContractScale, ShrinkScale>{first,
		                                                                              std::forward<F>(f)};
	}

	template <std::size_t N, typename InputIt, typename F, typename ReflectScale = std::ratio<1>,
	          typename ExpandScale = std::ratio_multiply<ReflectScale, std::ratio<2>>,
	          typename ContractScale = std::ratio_divide<ReflectScale, std::ratio<2>>,
	          typename ShrinkScale = ContractScale>
	auto constexpr make_amoeba(InputIt first, F&& f) {
		using T = typename std::iterator_traits<
		    typename std::iterator_traits<InputIt>::value_type::const_iterator>::value_type;
		return amoeba<N, T, F, ReflectScale, ExpandScale, ContractScale, ShrinkScale>{first,
		                                                                              std::forward<F>(f)};
	}


	template <std::size_t N, typename T, typename InputIt1, typename InputIt2, typename F,
	          typename ReflectScale = std::ratio<1>,
	          typename ExpandScale = std::ratio_multiply<ReflectScale, std::ratio<2>>,
	          typename ContractScale = std::ratio_divide<ReflectScale, std::ratio<2>>,
	          typename ShrinkScale = ContractScale,
	          std::enable_if_t<nxx::traits::is_iterator_v<InputIt2>, int> = 0>
	auto constexpr make_amoeba(InputIt1 first, InputIt2 diff_first, F&& f) {
		return amoeba<N, T, F, ReflectScale, ExpandScale, ContractScale, ShrinkScale>{first, diff_first,
		                                                                              std::forward<F>(f)};
	}

	template <std::size_t N, typename InputIt1, typename InputIt2, typename F,
	          typename ReflectScale = std::ratio<1>,
	          typename ExpandScale = std::ratio_multiply<ReflectScale, std::ratio<2>>,
	          typename ContractScale = std::ratio_divide<ReflectScale, std::ratio<2>>,
	          typename ShrinkScale = ContractScale,
	          std::enable_if_t<nxx::traits::is_iterator_v<InputIt2>, int> = 0>
	auto constexpr make_amoeba(InputIt1 first, InputIt2 diff_first, F&& f) {
		using T = std::invoke_result_t<std::plus<>, typename std::iterator_traits<InputIt1>::reference,
		                               typename std::iterator_traits<InputIt2>::reference>;
		return amoeba<N, T, F, ReflectScale, ExpandScale, ContractScale, ShrinkScale>{first, diff_first,
		                                                                              std::forward<F>(f)};
	}


	template <
	    std::size_t N, typename T, typename InputIt, typename S, typename F,
	    typename ReflectScale = std::ratio<1>,
	    typename ExpandScale = std::ratio_multiply<ReflectScale, std::ratio<2>>,
	    typename ContractScale = std::ratio_divide<ReflectScale, std::ratio<2>>,
	    typename ShrinkScale = ContractScale,
	    std::enable_if_t<std::negation_v<nxx::traits::is_iterator<std::remove_reference_t<S>>>, int> = 0>
	auto constexpr make_amoeba(InputIt first, S&& diff, F&& f) {
		return amoeba<N, T, F, ReflectScale, ExpandScale, ContractScale, ShrinkScale>{
		    first, std::forward<S>(diff), std::forward<F>(f)};
	}

	template <
	    std::size_t N, typename InputIt, typename S, typename F, typename ReflectScale = std::ratio<1>,
	    typename ExpandScale = std::ratio_multiply<ReflectScale, std::ratio<2>>,
	    typename ContractScale = std::ratio_divide<ReflectScale, std::ratio<2>>,
	    typename ShrinkScale = ContractScale,
	    std::enable_if_t<std::negation_v<nxx::traits::is_iterator<std::remove_reference_t<S>>>, int> = 0>
	auto constexpr make_amoeba(InputIt first, S&& diff, F&& f) {
		using T
		    = std::invoke_result_t<std::plus<>, typename std::iterator_traits<InputIt>::reference, S const&>;
		return amoeba<N, T, F, ReflectScale, ExpandScale, ContractScale, ShrinkScale>{
		    first, std::forward<S>(diff), std::forward<F>(f)};
	}

}  // namespace ppal::optimize

#endif  // ifndef PPAL__OPTIMIZE_HXX
