include(FetchContent)

# nxx
set(NXX_BUILD_TESTING OFF)
FetchContent_Declare(nxx
	GIT_REPOSITORY https://gitlab.com/parnmatt/nxx.git)
FetchContent_GetProperties(nxx)
if(NOT nxx_POPULATED)
	FetchContent_Populate(nxx)
	add_subdirectory(${nxx_SOURCE_DIR} ${nxx_BINARY_DIR})
endif()


# ROOT
if(DEFINED ENV{ROOTSYS})
	list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
endif()
find_package(ROOT REQUIRED)

# modify ROOT options into more usable form
set(ROOT_COMPLILE_OPTIONS ${ROOT_CXX_FLAGS})
separate_arguments(ROOT_COMPLILE_OPTIONS)

# make ROOT accessible as a library
add_library(ROOT INTERFACE IMPORTED GLOBAL)
target_link_libraries(ROOT INTERFACE ${ROOT_LIBRARIES})
target_include_directories(ROOT INTERFACE ${ROOT_INCLUDE_DIRS})
target_compile_definitions(ROOT INTERFACE ${ROOT_DEFINITIONS})
target_compile_options(ROOT INTERFACE ${ROOT_COMPLILE_OPTIONS})
add_library(ppal::ROOT ALIAS ROOT)


# export/install custom ROOT library
# horrible hack, but it works
export(TARGETS ROOT
	NAMESPACE ppal::
	FILE ${CMAKE_CURRENT_BINARY_DIR}/ROOT.cmake)

install(CODE
	"file(INSTALL ${CMAKE_CURRENT_BINARY_DIR}/ROOT.cmake
		DESTINATION  ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}/cmake/ppal)")
