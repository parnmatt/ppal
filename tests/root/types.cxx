#include <Rtypes.h>

#include <root/types.hxx>

#define PPAL_USE_STATIC_REQUIRE
#include "../testing.hxx"

TEST_CASE("ppal basic ROOT types implicitly convert to ROOT's native basic types", "[root][types]") {
	STATIC_REQUIRE((std::is_convertible_v<root::bool_t, Bool_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::byte_t, Byte_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::char_t, Char_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::uchar_t, UChar_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::short_t, Short_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::ushort_t, UShort_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::int_t, Int_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::uint_t, UInt_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::long_t, Long_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::ulong_t, ULong_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::long64_t, Long64_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::ulong64_t, ULong64_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::float_t, Float_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::float16_t, Float16_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::real_t, Real_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::double_t, Double_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::double32_t, Double32_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::long_double_t, LongDouble_t>));

	STATIC_REQUIRE((std::is_convertible_v<root::seek_t, Seek_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::text_t, Text_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::version_t, Version_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::option_t, Option_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::axis_t, Axis_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::stat_t, Stat_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::font_t, Font_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::style_t, Style_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::marker_t, Marker_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::width_t, Width_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::color_t, Color_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::screen_coord_t, SCoord_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::pad_coord_t, Coord_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::angle_t, Angle_t>));
	STATIC_REQUIRE((std::is_convertible_v<root::size_t, Size_t>));
}

TEST_CASE("ROOT's native basic types implicitly convert to ppals ROOT basic types", "[root][types]") {
	STATIC_REQUIRE((std::is_convertible_v<Bool_t, root::bool_t>));
	STATIC_REQUIRE((std::is_convertible_v<Byte_t, root::byte_t>));
	STATIC_REQUIRE((std::is_convertible_v<Char_t, root::char_t>));
	STATIC_REQUIRE((std::is_convertible_v<UChar_t, root::uchar_t>));
	STATIC_REQUIRE((std::is_convertible_v<Short_t, root::short_t>));
	STATIC_REQUIRE((std::is_convertible_v<UShort_t, root::ushort_t>));
	STATIC_REQUIRE((std::is_convertible_v<Int_t, root::int_t>));
	STATIC_REQUIRE((std::is_convertible_v<UInt_t, root::uint_t>));
	STATIC_REQUIRE((std::is_convertible_v<Long_t, root::long_t>));
	STATIC_REQUIRE((std::is_convertible_v<ULong_t, root::ulong_t>));
	STATIC_REQUIRE((std::is_convertible_v<Long64_t, root::long64_t>));
	STATIC_REQUIRE((std::is_convertible_v<ULong64_t, root::ulong64_t>));
	STATIC_REQUIRE((std::is_convertible_v<Float_t, root::float_t>));
	STATIC_REQUIRE((std::is_convertible_v<Float16_t, root::float16_t>));
	STATIC_REQUIRE((std::is_convertible_v<Real_t, root::real_t>));
	STATIC_REQUIRE((std::is_convertible_v<Double_t, root::double_t>));
	STATIC_REQUIRE((std::is_convertible_v<Double32_t, root::double32_t>));
	STATIC_REQUIRE((std::is_convertible_v<LongDouble_t, root::long_double_t>));

	STATIC_REQUIRE((std::is_convertible_v<Seek_t, root::seek_t>));
	STATIC_REQUIRE((std::is_convertible_v<Text_t, root::text_t>));
	STATIC_REQUIRE((std::is_convertible_v<Version_t, root::version_t>));
	STATIC_REQUIRE((std::is_convertible_v<Option_t, root::option_t>));
	STATIC_REQUIRE((std::is_convertible_v<Axis_t, root::axis_t>));
	STATIC_REQUIRE((std::is_convertible_v<Stat_t, root::stat_t>));
	STATIC_REQUIRE((std::is_convertible_v<Font_t, root::font_t>));
	STATIC_REQUIRE((std::is_convertible_v<Style_t, root::style_t>));
	STATIC_REQUIRE((std::is_convertible_v<Marker_t, root::marker_t>));
	STATIC_REQUIRE((std::is_convertible_v<Width_t, root::width_t>));
	STATIC_REQUIRE((std::is_convertible_v<Color_t, root::color_t>));
	STATIC_REQUIRE((std::is_convertible_v<SCoord_t, root::screen_coord_t>));
	STATIC_REQUIRE((std::is_convertible_v<Coord_t, root::pad_coord_t>));
	STATIC_REQUIRE((std::is_convertible_v<Angle_t, root::angle_t>));
	STATIC_REQUIRE((std::is_convertible_v<Size_t, root::size_t>));
}
