#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <type_traits>
#include <typeinfo>

#include <TF1.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <TParameter.h>

#include <nxx/ostream.hxx>

#include <root/file.hxx>

#define PPAL_USE_STATIC_REQUIRE
#include "../testing.hxx"

using namespace std::literals;
namespace fs = std::filesystem;
using Catch::Matchers::Contains;
using Catch::Matchers::Equals;


TEST_CASE("files can be default constructed", "[root][io][file]") {
	STATIC_REQUIRE(std::is_default_constructible_v<root::io::file<>>);
}

TEST_CASE("files cannot be copied or moved", "[root][io][file]") {
	STATIC_REQUIRE_FALSE(std::is_copy_constructible_v<root::io::file<>>);
	STATIC_REQUIRE_FALSE(std::is_copy_assignable_v<root::io::file<>>);
	STATIC_REQUIRE_FALSE(std::is_move_constructible_v<root::io::file<>>);
	STATIC_REQUIRE_FALSE(std::is_move_assignable_v<root::io::file<>>);
}

SCENARIO("files are created with a valid path", "[root][io][file][actual_io]") {
	GIVEN("a valid (generated) path") {
		auto filename = fs::path{std::tmpnam(nullptr) + ".root"s};

		WHEN("the user creates a file using the filename") {
			auto file = root::io::file{filename};

			THEN("the file should exist on the filesystem") { REQUIRE(fs::exists(filename)); }
		}
	}
}

SCENARIO("A ROOT TFile can be obtained from a file", "[root][io][file]") {
	GIVEN("an open file") {
		INFO("testing TMemFile as interface is the same; without IO");
		auto filename = "root_io_file_test_name"s;
		root::io::mem_file file{filename};

		WHEN("the user requests the ROOT TFile") {
			auto& tfile = *file;

			THEN("the locations of the file should match") {
				REQUIRE_THAT(tfile.GetName(), Equals(filename));
			}
		}
	}
}

SCENARIO("files have a name corresponding to their path", "[root][io][file]") {
	GIVEN("an open file") {
		INFO("testing TMemFile as interface is the same; without IO");
		auto filename = "root_io_file_test_name"s;
		root::io::mem_file file{filename};

		WHEN("the user requests the file's name") {
			auto name = file.name();

			THEN("the name should match the originally given filename") {
				REQUIRE_THAT(name, Equals(filename));
			}
		}
	}
}

SCENARIO("files can retrieve data stored in them", "[root][io][file][get]") {
	GIVEN("a readable file with a histogram stored within") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_hist = TH1D{"root_hist", "Title;x label;y label", 100, -6, 8};
		auto gaussian = TF1{"gaussian", "TMath::Gaus(x, 1, 2)", root_hist.GetXaxis()->GetXmin(),
		                    root_hist.GetXaxis()->GetXmax()};
		root_hist.FillRandom("gaussian");
		file->cd();
		root_hist.Write();

		WHEN("the user retrieves the histogram from the file") {
			auto hist = *file.get<TH1D>("root_hist");

			THEN("the histograms should the the same") {
				REQUIRE_THAT(hist.GetName(), Equals(root_hist.GetName()));
				REQUIRE_THAT(hist.GetTitle(), Equals(root_hist.GetTitle()));
				REQUIRE_THAT(hist.GetXaxis()->GetTitle(), Equals(root_hist.GetXaxis()->GetTitle()));
				REQUIRE_THAT(hist.GetYaxis()->GetTitle(), Equals(root_hist.GetYaxis()->GetTitle()));

				auto nbins = hist.GetNbinsX();
				REQUIRE(nbins == root_hist.GetNbinsX());
				REQUIRE(hist.GetXaxis()->GetXmin() == root_hist.GetXaxis()->GetXmin());
				REQUIRE(hist.GetXaxis()->GetXmax() == root_hist.GetXaxis()->GetXmax());
				for (decltype(nbins) i{}; i < nbins; ++i) {
					CAPTURE(i);
					CHECK(hist.GetBinContent(i) == root_hist.GetBinContent(i));
				}
			}
		}
	}
}

SCENARIO("files can return a value, if the types are compatable", "[root][io][file][get]") {
	GIVEN("a readable file with a TGraphErrors stored within") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};

		auto root_hist = TH1D{"root_hist", "Title;x label;y label", 100, -6, 8};
		auto gaussian = TF1{"gaussian", "TMath::Gaus(x, 1, 2)", root_hist.GetXaxis()->GetXmin(),
		                    root_hist.GetXaxis()->GetXmax()};
		root_hist.FillRandom("gaussian");

		auto root_graph = TGraphErrors{&root_hist};
		root_graph.SetName("root_graph");
		root_graph.SetTitle(root_hist.GetTitle());
		root_graph.GetXaxis()->SetTitle(root_hist.GetXaxis()->GetTitle());
		root_graph.GetYaxis()->SetTitle(root_hist.GetYaxis()->GetTitle());

		file->cd();
		root_graph.Write();

		WHEN("the user retrieves the graph from the file") {
			INFO("TGraphErrors is a derived class of TGraph");
			auto graph = *file.get<TGraph>("root_graph");

			THEN("the graphs should the the same") {
				REQUIRE_THAT(graph.GetName(), Equals(root_graph.GetName()));
				REQUIRE_THAT(graph.GetTitle(), Equals(root_graph.GetTitle()));
				REQUIRE_THAT(graph.GetXaxis()->GetTitle(), Equals(root_graph.GetXaxis()->GetTitle()));
				REQUIRE_THAT(graph.GetYaxis()->GetTitle(), Equals(root_graph.GetYaxis()->GetTitle()));

				auto n = graph.GetN();
				REQUIRE(n == root_graph.GetN());
				for (decltype(n) i{}; i < n; ++i) {
					CAPTURE(i);
					double x{}, y{}, rx{}, ry{};
					graph.GetPoint(i, x, y);
					root_graph.GetPoint(i, rx, ry);
					CHECK(x == rx);
					CHECK(y == ry);
				}
			}
		}
	}
}

SCENARIO("files can retrieve and extract data stored in them", "[root][io][file][get]") {
	GIVEN("a readable file with a parameter stored within") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_pi = 3.141592653578979;
		TParameter<double> param{"root_pi", root_pi};
		file->cd();
		param.Write();

		WHEN("the user asks for the value as a double") {
			auto pi = *file.get<double>("root_pi");

			THEN("the parameter will be unwrapped, and the internal value returned to the user") {
				REQUIRE(pi == root_pi);
			}
		}
	}
}

SCENARIO("files cannot return a value, if the types are not compatable", "[root][io][file][get]") {
	GIVEN("a readable file with a histogram stored within") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_dist = TH1D{"root_dist", "Title;x label;y label", 1, 0, 1};
		file->cd();
		root_dist.Write();

		WHEN("the user asks for a TGraph from the file") {
			// inline

			THEN("the file will throw a std::bad_cast") {
				INFO("root_dist is a TH1D, not a TGraph");
				REQUIRE_THROWS_AS(file.get<TGraph>("root_dist"), std::bad_cast);
			}
		}
	}
}

SCENARIO("files graciously fail when data not stored in the file is request", "[root][io][file][get]") {
	GIVEN("an empty, readable file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};

		WHEN("the user requests data from the file, that doesn't exist") {
			auto data = file.get<TH1D>("imaginary_hist");

			THEN("the data is not useable, but can be checked") {
				CHECK_FALSE(data.has_value());
				AND_THEN("will throw on use") { REQUIRE_THROWS(data.value()); }
			}
		}
	}
}


SCENARIO("User can toggle current writing directory", "[root][io][toggle_current_dir]") {
	GIVEN("two writable files")
	AND_GIVEN("four parameters, with the first already stored in the first file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file_1{"root_io_file_test_name_1"};
		root::io::mem_file file_2{"root_io_file_test_name_2"};

		TParameter<int> param1{"param1", 1};
		TParameter<int> param2{"param2", 2};
		TParameter<int> param3{"param3", 3};
		TParameter<int> param4{"param4", 4};

		file_1->cd();
		param1.Write();

		WHEN("the user toggles to the second file")
		AND_WHEN("the user writes the next two parameters") {
			{
				root::io::toggle_current_dir toggle{&*file_2};
				param2.Write();
				param3.Write();
			}  // toggle goes out of scope

			THEN("the user writes the last parameter") {
				param4.Write();

				AND_THEN("the first file contains the parameters 1 and 4") {
					REQUIRE(*file_1.get<int>("param1") == 1);
					REQUIRE(*file_1.get<int>("param4") == 4);
				}

				AND_THEN("the second file contains parameters 2 and 3") {
					REQUIRE(*file_2.get<int>("param2") == 2);
					REQUIRE(*file_2.get<int>("param3") == 3);
				}
			}
		}
	}
}


SCENARIO("files can store native ROOT objects by writing them to the file", "[root][io][file][write]") {
	GIVEN("a wriable file")
	AND_GIVEN("a histogram of a Gaussian distribution") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_hist = TH1D{"root_hist", "Title;x label;y label", 100, -6, 8};
		auto gaussian = TF1{"gaussian", "TMath::Gaus(x, 1, 2)", root_hist.GetXaxis()->GetXmin(),
		                    root_hist.GetXaxis()->GetXmax()};
		root_hist.FillRandom("gaussian");

		WHEN("the user writes the histogram to the file") {
			file.write(root_hist);

			THEN("the histogram should be within the file") {
				std::ostringstream ss{};
				{
					nxx::ostream::redirect rs{std::cout, ss};
					file->ls();
				}

				CHECK_THAT(ss.str(), Contains("OBJ: TH1D\troot_hist"));
				REQUIRE_THAT(ss.str(), Contains("KEY: TH1D\troot_hist"));

				AND_THEN("when reaquired, the histograms should match") {
					auto hist = dynamic_cast<TH1D&>(*file->Get("root_hist"));

					REQUIRE_THAT(hist.GetName(), Equals(root_hist.GetName()));
					REQUIRE_THAT(hist.GetTitle(), Equals(root_hist.GetTitle()));
					REQUIRE_THAT(hist.GetXaxis()->GetTitle(), Equals(root_hist.GetXaxis()->GetTitle()));
					REQUIRE_THAT(hist.GetYaxis()->GetTitle(), Equals(root_hist.GetYaxis()->GetTitle()));

					auto nbins = hist.GetNbinsX();
					REQUIRE(nbins == root_hist.GetNbinsX());
					REQUIRE(hist.GetXaxis()->GetXmin() == root_hist.GetXaxis()->GetXmin());
					REQUIRE(hist.GetXaxis()->GetXmax() == root_hist.GetXaxis()->GetXmax());
					for (decltype(nbins) i{}; i < nbins; ++i) {
						CAPTURE(i);
						CHECK(hist.GetBinContent(i) == root_hist.GetBinContent(i));
					}
				}
			}
		}
	}
}

SCENARIO("The key of a writen object can be independent of it's name", "[root][io][file][write]") {
	GIVEN("a writable file")
	AND_GIVEN("a simple blank histogram written to the file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_hist = TH1D{"root_hist", "Title;x label;y label", 1, 0, 1};
		file.write(root_hist, "different_key");

		WHEN("the user lists the content of file") {
			std::ostringstream ss{};
			{
				nxx::ostream::redirect rs{std::cout, ss};
				file->ls();
			}
			THEN("the histogram should be within the file with a different key to it's name") {
				CHECK_THAT(ss.str(), Contains("OBJ: TH1D\troot_hist"));
				REQUIRE_THAT(ss.str(), Contains("KEY: TH1D\tdifferent_key"));
			}
		}
	}
}

SCENARIO("files can store non-native ROOT objects by wrapping them in a TParameter before writing them to "
         "the file",
         "[root][io][file][write]") {
	GIVEN("a wriable file")
	AND_GIVEN("a variable containing a hardcoded value of pi") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_pi = 3.14159265358979;

		WHEN("the user writes the variable to the file") {
			file.write(root_pi, "root_pi");

			THEN("the variable should be wrapped in a TParameter") {
				AND_THEN("visible in the file") {
					std::ostringstream ss{};
					{
						nxx::ostream::redirect rs{std::cout, ss};
						file->ls();
					}
					REQUIRE_THAT(ss.str(), Contains("KEY: TParameter<double>\troot_pi"));
				}
			}
		}
	}
}

SCENARIO("Native ROOT types can be written to a file using a stream operator",
         "[root][io][file][write][stream]") {
	GIVEN("a writable file")
	AND_GIVEN("a simple blank histogram written to the file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_hist = TH1D{"root_hist", "Title;x label;y label", 1, 0, 1};

		WHEN("the user writes the histogram to the file") {
			file << root_hist;

			THEN("the histogram should be within the file") {
				std::ostringstream ss{};
				{
					nxx::ostream::redirect rs{std::cout, ss};
					file->ls();
				}
				REQUIRE_THAT(ss.str(), Contains("KEY: TH1D\troot_hist"));
			}
		}
	}
}

SCENARIO("files cannot be accessed when in an invalid state", "[root][io][file][is_valid]") {
	GIVEN("a filename of a file which does not exist on the filesystem") {
		auto filename = fs::path{std::tmpnam(nullptr) + ".root"s};

		WHEN("the user creates a read-only file using the filename") {
			root::io::file<root::io::openmode::in> file{filename};

			THEN("the file should indicate it is invalid") {
				REQUIRE_FALSE(file.is_valid());

				AND_THEN("throw if used") { REQUIRE_THROWS(file.name()); }
			}
		}
	}
}

SCENARIO("Closed files should be counted as invalid state", "[root][io][file][is_valid][close]") {
	GIVEN("a valid, open file containing some data") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		auto root_hist = TH1D{"root_hist", "Title;x label;y label", 100, -6, 8};
		auto root_pi = 3.14159265358979;
		file.write(root_hist);
		file.write(root_pi, "root_pi");

		WHEN("the user closes the file") {
			file.close();

			THEN("access to the file is invalidated") { REQUIRE_FALSE(file.is_valid()); }
		}
	}
}

SCENARIO("Readable files indicate they are indeed readable", "[root][io][file][is_readable]") {
	GIVEN("a valid, open, readable file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};

		WHEN("the user queries if the file is readable") {
			// inline

			THEN("the user is informed it is") { REQUIRE(file.is_readable()); }
		}
	}
}

SCENARIO("Closed readable files indicate they are no longer readable", "[root][io][file][is_readable]") {
	GIVEN("a valid, now closed, readable file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		file.close();

		WHEN("the user queries if the file is readable") {
			// inline

			THEN("the user is informed it is not") { REQUIRE_FALSE(file.is_readable()); }
		}
	}
}

SCENARIO("Readable files indicate they are indeed writable", "[root][io][file][is_writable]") {
	GIVEN("a valid, open, writable file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};

		WHEN("the user queries if the file is writable") {
			// inline

			THEN("the user is informed it is") { REQUIRE(file.is_writable()); }
		}
	}
}

SCENARIO("Closed writable files indicate they are no longer writable", "[root][io][file][is_writable]") {
	GIVEN("a valid, now closed, writable file") {
		INFO("testing TMemFile as interface is the same; without IO");
		root::io::mem_file file{"root_io_file_test_name"};
		file.close();

		WHEN("the user queries if the file is writable") {
			// inline

			THEN("the user is informed it is not") { REQUIRE_FALSE(file.is_writable()); }
		}
	}
}
